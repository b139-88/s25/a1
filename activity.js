//Fruits on sale
db.fruits.aggregate(
    [{ 
        $match: {
            onSale: true
        }
    },{
        $count: "fruitsOnSale"
    }
]);

//Enough Stock
db.fruits.aggregate(
    [{ 
        $match: {
            stock: {
                $gte: 20
            }
        }
    },{
        $count: "enoughStock"
    }
]);

//Average price
db.fruits.aggregate(
    [{ 
        $match: {
            onSale: true
        }
    },{
        $group: {
            _id: "$supplier_id",
            avg_price: {
                $avg: "$price"
            }
        }
    },
    {
        $sort : {
            total: 1
        }
    }
]);

//Max price
db.fruits.aggregate(
    [{ 
        $match: {
            onSale: true
        }
    },{
        $group: {
            _id: "$supplier_id",
            max_price: {
                $max: "$price"
            }
        }
    },
    {
        $sort : {
            total: -1
        }
    }
]);

//Min price
db.fruits.aggregate(
    [{ 
        $match: {
            onSale: true
        }
    },{
        $group: {
            _id: "$supplier_id",
            min_price: {
                $min: "$price"
            }
        }
    },
    {
        $sort : {
            total: -1
        }
    }
]);